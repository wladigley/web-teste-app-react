import React, { Component } from 'react'

export default class FormComponent extends Component {
constructor(props){
    super(props);
    this.state={
        nome:'teste'
    }
}

componentDidUpdate(prevProps){
    if (this.props.nome !== prevProps.nome) {
        this.setState({nome:this.props.nome});
      }
}

setNewName = (event) => {
    this.setState({nome: event.target.value});
}
    render() {
        return (
            <div>
            <form>
                <fieldset>
                    <legend>Personalia:</legend>
                    <label>Last name:</label>
                    <input type="text" id="lname" name="lname" value={this.state.nome} onChange={this.setNewName}/><br/><br/>
                </fieldset>
            </form>                
            </div>
        )
    }
}

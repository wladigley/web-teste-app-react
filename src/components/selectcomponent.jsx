import React, { Component } from 'react'
import FormComponent from "./formcomponent";

function Getpessoas(){
    return [
        {
            nome:'Wladigley',
            idade: 10,
            telefone:'123456'
        },
        {
            nome:'Rodrigo',
            idade: 10,
            telefone:'123456'
        },
        {
            nome:'Atila',
            idade: 10,
            telefone:'123456'
        },
        {
            nome:'Luiz',
            idade: 10,
            telefone:'123456'
        },
    ]

}


export default class SelectComponent extends Component {
    constructor(props){
        super(props);
        this.state = {
            nome:""
        };
    }

    setNameSelect = (e)=>{
        this.setState({nome : e.target.value});
    }

    render() {
        return (
            <div>
                <label>Choose a Person:</label>
                <select name="pessoas" id="pessoas" onChange={this.setNameSelect} >
                    {
                        Getpessoas().map((list,index)=>(
                            <option key={index} value={list.nome}  >{list.nome}</option>
                        ))
                    }
                </select>
                <FormComponent nome={this.state.nome}/>                 
            </div>
        )
    }
}